This dataset provides data from every country about the estimated fatalities. It also includes road fatalities with motor vehicles and inhabitants of every country. As you might have expected, China is the most fatal country regarding traffic accidents.

## Data Preparation

##### Sources
The data is sourced from 
* https://en.wikipedia.org/wiki/List_of_countries_by_traffic-related_death_rate

##### Requirements
The data preparation was performed using Python Programming Language version 3.8. running on a AMD A10 (64-bit) windows on a Lenovo Z50 laptop.

##### Processing 
The data was scraped and cleaned up with a script called "process.py" which will be located in the process folder of this repo.

##### Necessary python libraries for this: 
* bs4
* csv
* requests
* lxml

##### Instructions:
* Download ["requirements.txt"](https://gitlab.com/robgrootjen/traffic-fatalities-per-country/raw/master/Process/requirements.txt?inline=false)
* Go to your terminal and use the following command ```pip install requirements.txt```
* Copy [process.py script](https://gitlab.com/robgrootjen/traffic-fatalities-per-country/blob/master/Process/process.py)
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Paste and run the code
* 1 CSV file named "DeathByTraffic.csv" will be saved in document where your terminal is at the moment.

##### Download Raw CSV Data
* [Click here](https://gitlab.com/robgrootjen/traffic-fatalities-per-country/raw/master/Data/DeathByTraffic.csv?inline=false)

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0.

<a href="https://ibb.co/q5Y0rDQ"><img src="https://i.ibb.co/nCM3LwJ/newplot.png" alt="newplot" border="0"></a>